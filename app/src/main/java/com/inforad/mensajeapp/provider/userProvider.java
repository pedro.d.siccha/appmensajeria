package com.inforad.mensajeapp.provider;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.inforad.mensajeapp.model.User;

public class userProvider {

    DatabaseReference mDatabase;
    FirebaseAuth mAuth;

    public userProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
    }

    public Task<Void> crear(User user){
        return mDatabase.child(mAuth.getCurrentUser().getUid()).setValue(user);
    }

    public DatabaseReference obtenerUsuario(String idUsuario) {
        return mDatabase.child(idUsuario);
    }

}
