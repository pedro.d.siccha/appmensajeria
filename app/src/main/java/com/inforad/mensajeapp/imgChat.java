package com.inforad.mensajeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class imgChat extends AppCompatActivity {

    private String url;
    private ImageView btnCerrar, imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_chat);
        Inicio();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void Inicio() {
        url = getIntent().getStringExtra("urlImg");

        btnCerrar = findViewById(R.id.ivCerrar);
        imagen = findViewById(R.id.ivImagen);

        Picasso.with(this).load(url).into(imagen);

    }

}