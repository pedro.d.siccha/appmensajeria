package com.inforad.mensajeapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//import com.firebase.ui.auth.data.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.mensajeapp.model.User;
import com.inforad.mensajeapp.provider.userProvider;
import com.inforad.mensajeapp.utilidades.CompresorBitmapImage;
import com.inforad.mensajeapp.utilidades.FileUtilidades;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class datosPersonales extends AppCompatActivity {

    private EditText inputNombre, inputApellido;
    private Button btnSiguiente;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private userProvider mUserProvider;
    private CircleImageView btnSubirImagen;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_datos_personales);
        Iniciar();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressDialog.setMessage("Cargando Imagen...");
                mProgressDialog.setTitle("Espere...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                if (mImageFile != null){
                    String nomImagen = inputNombre.getText().toString().replace(" ","");
                    guardarImagen(nomImagen);
                }else {
                    String img = "https://firebasestorage.googleapis.com/v0/b/fixallapp-a57f9.appspot.com/o/imgUsuario%2Fdefault%2Fhombre.png?alt=media&token=a8bb773e-24b5-4659-a30e-f2be53e9ef50";
                    llamarVerifNumTelf(img);
                }
            }
        });

        btnSubirImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarGaleria();
            }
        });

    }

    private void Iniciar() {
        inputNombre = findViewById(R.id.inputNombre);
        inputApellido = findViewById(R.id.inputApellido);

        btnSubirImagen = findViewById(R.id.civPerfil);

        btnSiguiente = findViewById(R.id.bSiguiente);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserProvider = new userProvider();

        mProgressDialog = new ProgressDialog(datosPersonales.this);

    }

    private void mostrarGaleria() {
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                btnSubirImagen.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
                // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void guardarImagen(String alias){
        byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
        final StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgUsuario").child(mAuth.getCurrentUser().getUid()).child(alias + ".jpg");
        UploadTask uploadTask = storage.putBytes(imageByte);
        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()){
                    storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String img = uri.toString();
                            llamarVerifNumTelf(img);
                        }
                    });
                }else {
                    // Toast.makeText(viewDatosPersonales.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void llamarVerifNumTelf(String urlImagen){

        String user_id = mAuth.getCurrentUser().getUid();
        String nombre = inputNombre.getText().toString();
        String apellido = inputApellido.getText().toString();
        String imgUsuario = urlImagen;

        User user = new User(user_id, nombre, apellido, imgUsuario);
        mUserProvider.crear(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
        mProgressDialog.dismiss();

                Intent intent = new Intent(datosPersonales.this, home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }
        });
    }

}