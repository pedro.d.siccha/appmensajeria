package com.inforad.mensajeapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.inforad.mensajeapp.adaptador.mensajeAdaptador;
import com.inforad.mensajeapp.model.Mensaje;
import com.inforad.mensajeapp.provider.chatProvider;
import com.inforad.mensajeapp.provider.mensajeProvider;
import com.inforad.mensajeapp.provider.userProvider;
import com.inforad.mensajeapp.utilidades.CompresorBitmapImage;
import com.inforad.mensajeapp.utilidades.FileUtilidades;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Date;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class chatActivity extends AppCompatActivity {

    private String idEnvia, idRecibe, idUsuario, aliasEnvia, aliasRecive, imgEnvia;
    private chatProvider mChatProvider;
    private mensajeProvider mMensajeProvider;
    private View mActionBarView;
    private DatabaseReference mDatabase;
    private EditText inputMensaje;
    private ImageView btnEnviar, btnAtras;
    private CircleImageView imgPerfil, btnEnviarImagen;
    private TextView txtNombre, txtRelativeTime;
    private FirebaseAuth mAuth;
    private userProvider mUserProvider;
    private RecyclerView listaChat;
    private mensajeAdaptador mMensajeAdapter;
    private LinearLayoutManager linearLayoutManager;
    private int idNotificacion;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private Dialog mDialog;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Iniciar();
        mostrarToolbar(R.layout.custom_chat_toolbar);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarMensaje();
                //enviarImagen();
            }
        });

        btnEnviarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria();
            }
        });

    }

    private void abrirGaleria() {
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {

                mImageFile = FileUtilidades.from(this, data.getData());
                mostrarImgEnviar(mImageFile);
                //          imgFlyer.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            }catch (Exception e){
            }
        }
    }

    private void mostrarToolbar(int custom_toolbar) {
        Toolbar toolbar = findViewById(R.id.idToolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActionBarView = inflater.inflate(custom_toolbar, null);
        actionBar.setCustomView(mActionBarView);
        imgPerfil = mActionBarView.findViewById(R.id.civImgPerfil);
        btnAtras = mActionBarView.findViewById(R.id.ivAtras);
        txtNombre = mActionBarView.findViewById(R.id.tvUsuario);
        txtRelativeTime = mActionBarView.findViewById(R.id.tvRelativeTime);
        obtenerDatosUsuario();
        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void mostrarImgEnviar(File imgFile){
        TextView btnCerrar;
        ImageView imgEnviar;
        LinearLayout btnEnviarImg;

        btnCerrar = mDialog.findViewById(R.id.tvCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        imgEnviar = mDialog.findViewById(R.id.ivImgEnviar);
        imgEnviar.setImageBitmap(BitmapFactory.decodeFile(imgFile.getAbsolutePath()));
        btnEnviarImg = mDialog.findViewById(R.id.llEnviar);
        btnEnviarImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                mProgressDialog.setMessage("Enviando Imagen ...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                enviarImagen();

            }
        });
        mDialog.show();
    }

    private void enviarMensaje() {
        final String mensaje = inputMensaje.getText().toString();
        if (!mensaje.isEmpty()){
            mChatProvider.obtenerDatosChat(idEnvia, idRecibe).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()){
                        Mensaje msj = new Mensaje();
                        msj.setIdChat(snapshot.child("id").getValue().toString());
                        msj.setIdEnvia(idEnvia);
                        msj.setIdRecibe(idRecibe);
                        msj.setMensaje(mensaje);
                        msj.setVisto(false);
                        msj.setTimestamp(new Date().getTime());
                        mMensajeProvider.crearMensaje(msj).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                inputMensaje.setText("");
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void enviarImagen() {
        final String mensaje = inputMensaje.getText().toString();
        String idImagen = mDatabase.push().getKey();
        if (mImageFile.getPath() != null){
            byte[] imageByte = CompresorBitmapImage.getImage(this, mImageFile.getPath(), 500, 500);
            final StorageReference storage = FirebaseStorage.getInstance().getReference().child("imgChat").child(mAuth.getCurrentUser().getUid()).child(idImagen + ".jpg");
            UploadTask uploadTask = storage.putBytes(imageByte);
            uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if (task.isSuccessful()){
                        storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                final String img = uri.toString();

                                if (img.isEmpty()){
                                    enviarMensaje();
                                }

                                mChatProvider.obtenerDatosChat(idEnvia, idRecibe).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()){
                                            Mensaje msj = new Mensaje();
                                            msj.setIdChat(snapshot.child("id").getValue().toString());
                                            msj.setIdEnvia(idEnvia);
                                            msj.setIdRecibe(idRecibe);
                                            msj.setMensaje(mensaje);
                                            msj.setVisto(false);
                                            msj.setTimestamp(new Date().getTime());
                                            msj.setImagen(img);
                                            mMensajeProvider.crearMensaje(msj).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    inputMensaje.setText("");
                                                    mImageFile = new File("");
                                                    mProgressDialog.dismiss();

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                            }
                        });
                    }
                }
            });
        }else {
            enviarMensaje();
        }

    }

    private void obtenerDatosUsuario() {
        String idUsuarioInfo = "";
        if (idUsuario.equals(idEnvia)){
            idUsuarioInfo = idRecibe;
        }else{
            idUsuarioInfo = idEnvia;
        }

        mUserProvider.obtenerUsuario(idUsuarioInfo).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String img = snapshot.child("img").getValue().toString();
                    Picasso.with(chatActivity.this).load(img).into(imgPerfil);
                    aliasRecive = snapshot.child("nombre").getValue().toString();
                    txtNombre.setText(aliasRecive);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void Iniciar() {
        inputMensaje = findViewById(R.id.etMensaje);
        btnEnviar = findViewById(R.id.ivEnviar);
        btnEnviarImagen = findViewById(R.id.civEnviarImagen);
        listaChat = findViewById(R.id.rvMensaje);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        listaChat.setLayoutManager(linearLayoutManager);
        mChatProvider = new chatProvider();
        mMensajeProvider = new mensajeProvider();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mUserProvider = new userProvider();
        mProgressDialog = new ProgressDialog(chatActivity.this);

        mDialog = new Dialog(chatActivity.this);
        mDialog.setContentView(R.layout.modal_enviarimg);

        cargarDatos();
        crearChat();
    }

    private void crearChat() {
        Random random = new Random();
        int n = random.nextInt(10000);
        mChatProvider.obtenerOrdenUsuario(idEnvia, idRecibe, chatActivity.this, n);
    }

    private void cargarDatos() {
        idEnvia = getIntent().getStringExtra("idEnvia");
        idRecibe = getIntent().getStringExtra("idRecibir");

        mUserProvider.obtenerUsuario(idUsuario).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    aliasEnvia = snapshot.child("nombre").getValue().toString();
                    imgEnvia = snapshot.child("img").getValue().toString();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mChatProvider.obtenerDatosChat(idEnvia, idRecibe).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String idChat = snapshot.child("id").getValue().toString();
                    Query queryMensaje = mDatabase.child("Mensajes").child(idChat).orderByChild("timestamp");
                    FirebaseRecyclerOptions<Mensaje> options = new FirebaseRecyclerOptions.Builder<Mensaje>().setQuery(queryMensaje, Mensaje.class).build();
                    mMensajeAdapter = new mensajeAdaptador(options, chatActivity.this);
                    listaChat.setAdapter(mMensajeAdapter);
                    mMensajeAdapter.startListening();
                    mMensajeAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                        @Override
                        public void onItemRangeChanged(int positionStart, int itemCount) {
                            super.onItemRangeChanged(positionStart, itemCount);
                            int numeroMensajes = mMensajeAdapter.getItemCount();
                            int ultimoMensajePosicion = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                            if (ultimoMensajePosicion == -1 || (positionStart >= (numeroMensajes - 1 ) && ultimoMensajePosicion == (positionStart - 1))){
                                listaChat.scrollToPosition(positionStart);
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}