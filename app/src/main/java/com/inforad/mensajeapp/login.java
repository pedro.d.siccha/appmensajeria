package com.inforad.mensajeapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.firebase.ui.auth.AuthMethodPickerLayout;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

public class login extends AppCompatActivity {

    private static final int RC_SING_IN = 123;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private DatabaseReference mdataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        Iniciar();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null){
                    String id_registro = user.getUid();

                    mdataBase.child("Users").child(id_registro).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                Toast.makeText(login.this, "Bienvenido " + user.getDisplayName(), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(login.this, home.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }else {

                                Intent intent = new Intent(login.this, datosPersonales.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            //Toast.makeText(viewLogin.this, "Error: " + databaseError, Toast.LENGTH_SHORT).show();
                        }
                    });

                }else {


                    AuthUI.IdpConfig googleidp = new AuthUI.IdpConfig.GoogleBuilder().build();
                    AuthMethodPickerLayout customLayout = new AuthMethodPickerLayout.Builder(R.layout.activity_view_pregunta_acceso).setEmailButtonId(R.id.btnCorreoPa).setGoogleButtonId(R.id.btnGooglePa).setTosAndPrivacyPolicyId(R.id.tvPoliticas).build();

                    startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().setIsSmartLockEnabled(false).setTosAndPrivacyPolicyUrls("http://databaseremote.esy.es/RegisterLite/html/privacidad.html", "http://databaseremote.esy.es/RegisterLite/html/privacidad.html").setAvailableProviders(Arrays.asList(new AuthUI.IdpConfig.EmailBuilder().build(), googleidp)).setAuthMethodPickerLayout(customLayout).build(), RC_SING_IN);

                }
            }
        };
    }

    private void Iniciar() {
        mAuth = FirebaseAuth.getInstance();
        mdataBase = FirebaseDatabase.getInstance().getReference();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_SING_IN){
            if (resultCode == RESULT_OK){
                //Toast.makeText(viewLogin.this,  "Bienvendio", Toast.LENGTH_LONG).show();
            }else {
                //Toast.makeText(viewLogin.this, "Algo Salio mal " + RESULT_OK, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthStateListener != null){
            mAuth.removeAuthStateListener(mAuthStateListener);
        }
    }
}