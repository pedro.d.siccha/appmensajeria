package com.inforad.mensajeapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.inforad.mensajeapp.adaptador.contactosAdaptador;
import com.inforad.mensajeapp.adaptador.userAdaptador;
import com.inforad.mensajeapp.model.Chat;
import com.inforad.mensajeapp.model.User;
import com.inforad.mensajeapp.provider.chatProvider;

public class home extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private RecyclerView rListaContactos, rListaUsuarios;
    private String idUsuario, idChat;
    private contactosAdaptador mContactoAdaptador;
    private userAdaptador mUserAdaptador;
    private chatProvider mChatProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Inicio();
    }

    private void Inicio() {

        mAuth = FirebaseAuth.getInstance();
        idUsuario = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mChatProvider = new chatProvider();

        rListaContactos = findViewById(R.id.rvContactos);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rListaContactos.setLayoutManager(linearLayoutManager);

        rListaUsuarios = findViewById(R.id.rvUsuarios);
        LinearLayoutManager linearLayoutManagerUsuarios = new LinearLayoutManager(this);
        rListaUsuarios.setLayoutManager(linearLayoutManagerUsuarios);

    }

    @Override
    protected void onStart() {
        super.onStart();

        Query queryContacto = mDatabase.child("chats").child(idUsuario).child("Users");
        FirebaseRecyclerOptions<Chat> options = new FirebaseRecyclerOptions.Builder<Chat>().setQuery(queryContacto, Chat.class).build();
        mContactoAdaptador = new contactosAdaptador(options, home.this);
        rListaContactos.setAdapter(mContactoAdaptador);
        mContactoAdaptador.startListening();



        Query queryUsuario = mDatabase.child("Users");
        FirebaseRecyclerOptions<User> optionsU = new FirebaseRecyclerOptions.Builder<User>().setQuery(queryUsuario, User.class).build();
        mUserAdaptador = new userAdaptador(optionsU, home.this);
        rListaUsuarios.setAdapter(mUserAdaptador);
        mUserAdaptador.startListening();

    }
}