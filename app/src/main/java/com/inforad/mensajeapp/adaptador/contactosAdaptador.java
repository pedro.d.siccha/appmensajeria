package com.inforad.mensajeapp.adaptador;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.inforad.mensajeapp.R;
import com.inforad.mensajeapp.chatActivity;
import com.inforad.mensajeapp.model.Chat;
import com.inforad.mensajeapp.provider.chatProvider;
import com.inforad.mensajeapp.provider.userProvider;
import com.squareup.picasso.Picasso;

import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class contactosAdaptador extends FirebaseRecyclerAdapter<Chat, contactosAdaptador.ViewHolder> {

    private Context mContext;

    public contactosAdaptador(@NonNull FirebaseRecyclerOptions<Chat> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, int position, @NonNull final Chat chatModel) {

        holder.mUserProvider.obtenerUsuario(chatModel.getIdUsuario2()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Picasso.with(mContext).load(snapshot.child("img").getValue().toString()).into(holder.imgPerfil);

                    holder.txtNombre.setText(snapshot.child("nombre").getValue().toString());
                    holder.txtApellido.setText(snapshot.child("apellido").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, chatActivity.class);
                intent.putExtra("idEnvia", chatModel.getIdUsuario1());
                intent.putExtra("idRecibir", chatModel.getIdUsuario2());
                mContext.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacto, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private ImageView btnChat;
        private TextView txtNombre, txtApellido;
        private View mView;
        private chatProvider mChatProvider;
        private FirebaseAuth mAuth;
        private String idUsuario;
        private userProvider mUserProvider;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civImgPerfil);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtApellido = view.findViewById(R.id.tvApellido);
            btnChat = view.findViewById(R.id.ivChat);
            mChatProvider = new chatProvider();
            mAuth = FirebaseAuth.getInstance();
            idUsuario = mAuth.getCurrentUser().getUid();
            mUserProvider = new userProvider();
        }
    }

}
