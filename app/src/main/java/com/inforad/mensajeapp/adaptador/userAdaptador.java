package com.inforad.mensajeapp.adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.inforad.mensajeapp.R;
import com.inforad.mensajeapp.model.Chat;
import com.inforad.mensajeapp.model.User;
import com.inforad.mensajeapp.provider.chatProvider;
import com.inforad.mensajeapp.provider.userProvider;
import com.squareup.picasso.Picasso;

import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class userAdaptador extends FirebaseRecyclerAdapter<User, userAdaptador.ViewHolder> {

    private Context mContext;

    public userAdaptador(@NonNull FirebaseRecyclerOptions<User> options, Context mContext) {
        super(options);
        this.mContext = mContext;
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, int position, @NonNull final User userModel) {
        Picasso.with(mContext).load(userModel.getImg()).into(holder.imgPerfil);

        holder.txtNombre.setText(userModel.getNombre());
        holder.txtApellido.setText(userModel.getApellido());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "El usuario " + userModel.getNombre() + ", fue agregado a sus contactos.", Toast.LENGTH_SHORT).show();

                Random random =new Random();
                int n =random.nextInt(10000);
                holder.mChatProvider.obtenerOrdenUsuario(holder.idUsuario, userModel.getId(), mContext, n);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuario, parent, false);
        return new ViewHolder(view);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView imgPerfil;
        private ImageView btnAgregar;
        private TextView txtNombre, txtApellido;
        private View mView;
        private chatProvider mChatProvider;
        private FirebaseAuth mAuth;
        private String idUsuario;

        public ViewHolder(View view){
            super(view);
            mView = view;
            imgPerfil = view.findViewById(R.id.civImgPerfil);
            txtNombre = view.findViewById(R.id.tvNombre);
            txtApellido = view.findViewById(R.id.tvApellido);
            btnAgregar = view.findViewById(R.id.ivAgregar);
            mChatProvider = new chatProvider();
            mAuth = FirebaseAuth.getInstance();
            idUsuario = mAuth.getCurrentUser().getUid();
        }
    }
}
